#include "tim2.h"
long long int a;
typedef struct b8{
long long int a;
} B8;
typedef struct b128{
long long int a[16];
} B128;
typedef struct b1024{
long long int a[64];
	long long int b[64];
} B1024;

B8 t8;
B128 t128;

double test_rand_32_add(void) 
{
	  
	  uint16_t start,bet=0;
		long int rand1,rand2;
	  long long int result;
	for(int i=0;i<100;i++)
	{
	  rand1 = rand_32();
	  rand2 = rand_32();
	
	  start = timer_start();  //there are 24 clocks if stop immidiately after called start
		result = rand1+rand2;
	  bet += timer_stop(start);
	
	  a = result;
	}
	  return ((double)(bet)/100.0);
}
double test_rand_64_add(void)
{
		uint16_t start,bet=0;
		long long int rand1,rand2;
	  long long int result;
	for(int i=0;i<100;i++)
	{
	  rand1 = rand_64();
	  rand2 = rand_64();
	
	  start = timer_start();  //there are 22 clocks if stop immidiately after called start
	  result = rand1+rand2;
	  bet += timer_stop(start);
	
	  a = result;
	}
	  return ((double)(bet)/100.0);
}
double test_rand_32_mul(void)
{
		uint16_t start,bet=0;
		long int rand1,rand2;
	  long long int result;
	for(int i=0;i<100;i++)
	{
	  rand1 = rand_32();
	  rand2 = rand_32();
	
	  start = timer_start();  //there are 24 clocks if stop immidiately after called start
		result = rand1*rand2;
	  bet += timer_stop(start);
	
	  a = result;
	}
	  return ((double)(bet)/100.0);
}
double test_rand_64_mul(void)
{
	  uint16_t start,bet=0;
		long long int rand1,rand2;
	  long long int result;
	for(int i=0;i<100;i++)
	{
	  rand1 = rand_64();
	  rand2 = rand_64();
	
	  start = timer_start();  //there are 22 clocks if stop immidiately after called start
	  result = rand1*rand2;
	  bet += timer_stop(start);
	
	  a = result;
	}
	  return ((double)(bet)/100.0);
}
double test_rand_32_div(void)
{
		uint16_t start,bet=0;
		long int rand1,rand2;
	  long long int result;
	for(int i=0;i<100;i++)
	{
	  rand1 = rand_32();
	  rand2 = rand_32();
	  while(rand2==0)
		{
			rand2 = rand_32();
		}
	  start = timer_start();  //there are 24 clocks if stop immidiately after called start
		result = rand1/rand2;
	  bet += timer_stop(start);
	
	  a = result;
	}
	  return ((double)(bet)/100);
}
double test_rand_64_div(void)
{
		uint16_t start,bet=0;
		long long int rand1,rand2;
	  long long int result;
	for(int i=0;i<100;i++)
	{
	  rand1 = rand_64();
	  rand2 = rand_64();
		while(rand2==0)
		{
			rand2 = rand_64();
		}
	  start = timer_start();  //there are 22 clocks if stop immidiately after called start
	  result = rand1/rand2;
	  bet += timer_stop(start);
	
	  a = result;
	}
	  return ((double)(bet)/100);
}
double test_struct_8_copy(void)
{
	uint16_t start,bet=0;
	B8 a,b;
	for(int i=0;i<100;i++)
	{
	
		a.a = rand_64();
	
	  start = timer_start();  //there are 22 clocks if stop immidiately after called start
	  b=a;
	  bet += timer_stop(start);
  }
	t8 = b;
	return ((double)(bet)/100.0);
}
double test_struct_128_copy(void)
{
	uint16_t start,bet=0;
	B128 a,b;
	for(int i=0;i<100;i++)
	{
	for(int j=0;j<16;j++)
	{
		a.a[j] = 0;
	}
	  start = timer_start();  //there are 22 clocks if stop immidiately after called start
	  b=a;
	  bet += timer_stop(start);
  }
	t128 = b;
	return ((double)(bet)/100);
}
double test_struct_1024_copy(void)
{
//  uint16_t start,bet=0;
//	B1024 a,b;

//	for(int j=0;j<64;j++)
//	{
//		a.a[j] = j;
//		a.b[j] = j;
//	}
//	  start = timer_start();  //there are 22 clocks if stop immidiately after called start
//	  //b=a;
//	 bet += timer_stop(start);
//	 // b.a[1]++;
//	return ((double)(b.a[63]));
	return 0;
}
