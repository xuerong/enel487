/*
    FreeRTOS V7.2.0 - Copyright (C) 2012 Real Time Engineers Ltd.


    ***************************************************************************
     *                                                                       *
     *    FreeRTOS tutorial books are available in pdf and paperback.        *
     *    Complete, revised, and edited pdf reference manuals are also       *
     *    available.                                                         *
     *                                                                       *
     *    Purchasing FreeRTOS documentation will not only help you, by       *
     *    ensuring you get running as quickly as possible and with an        *
     *    in-depth knowledge of how to use FreeRTOS, it will also help       *
     *    the FreeRTOS project to continue with its mission of providing     *
     *    professional grade, cross platform, de facto standard solutions    *
     *    for microcontrollers - completely free of charge!                  *
     *                                                                       *
     *    >>> See http://www.FreeRTOS.org/Documentation for details. <<<     *
     *                                                                       *
     *    Thank you for using FreeRTOS, and thank you for your support!      *
     *                                                                       *
    ***************************************************************************


    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    >>>NOTE<<< The modification to the GPL is included to allow you to
    distribute a combined work that includes FreeRTOS without being obliged to
    provide the source code for proprietary components outside of the FreeRTOS
    kernel.  FreeRTOS is distributed in the hope that it will be useful, but
    WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
    or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public
    License and the FreeRTOS license exception along with FreeRTOS; if not it
    can be viewed here: http://www.freertos.org/a00114.html and also obtained
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!
    
    ***************************************************************************
     *                                                                       *
     *    Having a problem?  Start by reading the FAQ "My application does   *
     *    not run, what could be wrong?                                      *
     *                                                                       *
     *    http://www.FreeRTOS.org/FAQHelp.html                               *
     *                                                                       *
    ***************************************************************************

    
    http://www.FreeRTOS.org - Documentation, training, latest information, 
    license and contact details.
    
    http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
    including FreeRTOS+Trace - an indispensable productivity tool.

    Real Time Engineers ltd license FreeRTOS to High Integrity Systems, who sell 
    the code with commercial support, indemnification, and middleware, under 
    the OpenRTOS brand: http://www.OpenRTOS.com.  High Integrity Systems also
    provide a safety engineered and independently SIL3 certified version under 
    the SafeRTOS brand: http://www.SafeRTOS.com.
*/

/*
 * This is a very simple demo that demonstrates task and queue usages only in
 * a simple and minimal FreeRTOS configuration.  Details of other FreeRTOS 
 * features (API functions, tracing features, diagnostic hook functions, memory
 * management, etc.) can be found on the FreeRTOS web site 
 * (http://www.FreeRTOS.org) and in the FreeRTOS book.
 *
 * Details of this demo (what it does, how it should behave, etc.) can be found
 * in the accompanying PDF application note.
 *
*/

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
/* Standard include. */
#include <stdio.h>

/* Priorities at which the tasks are created. */
#define mainQUEUE_RECEIVE_TASK_PRIORITY     ( tskIDLE_PRIORITY + 2 )
#define mainQUEUE_SEND_TASK_PRIORITY        ( tskIDLE_PRIORITY + 1 )
#define mainTASK1_PRIORITY                  ( tskIDLE_PRIORITY + 3 )
#define mainTASK0_PRIORITY                  ( tskIDLE_PRIORITY + 4 )

#define plant_Priority       								  ( tskIDLE_PRIORITY + 1 )
#define control_Priority       								  ( tskIDLE_PRIORITY + 3 )
#define IU_Priority       								  ( tskIDLE_PRIORITY + 21 )
#define ADC_Priority       								  ( tskIDLE_PRIORITY + 4 )
/* The rate at which data is sent to the queue, specified in milliseconds. */
#define mainQUEUE_SEND_FREQUENCY_MS         ( 10 / portTICK_RATE_MS )

/* The number of items the queue can hold.  This is 1 as the receive task
will remove items as they are added, meaning the send task should always find
the queue empty. */
#define mainQUEUE_LENGTH                    ( 1 )

/* The ITM port is used to direct the printf() output to the serial window in 
the Keil simulator IDE. */
#define mainITM_Port8(n)    (*((volatile unsigned char *)(0xE0000000+4*n)))
#define mainITM_Port32(n)   (*((volatile unsigned long *)(0xE0000000+4*n)))
#define mainDEMCR           (*((volatile unsigned long *)(0xE000EDFC)))
#define mainTRCENA          0x01000000

/*-----------------------------------------------------------*/

/*
 * The tasks as described in the accompanying PDF application note.
 */
//static void prvQueueReceiveTask( void *pvParameters );
//static void prvQueueSendTask( void *pvParameters );
//static void task0( void *pvParameters );
//static void task1( void *pvParameters );

static void simPlantTask(void *pvParameters);
static void UITask(void *pvParameters);
static void ControlTask(void *pvParameters);
static void ADCTask(void *pvParameters);
/*
 * Redirects the printf() output to the serial window in the Keil simulator
 * IDE.
 */
int fputc( int iChar, FILE *pxNotUsed );

/*-----------------------------------------------------------*/

/* The queue used by both tasks. */
//static xQueueHandle xQueue = NULL;
static xQueueHandle xPlant = NULL;
static xQueueHandle xControlin = NULL;
/* One array position is used for each task created by this demo.  The 
variables in this array are set and cleared by the trace macros within
FreeRTOS, and displayed on the logic analyzer window within the Keil IDE -
the result of which being that the logic analyzer shows which task is
running when. */
unsigned long ulTaskNumber[ configEXPECTED_NO_RUNNING_TASKS ];

/*-----------------------------------------------------------*/
//#include "main.h"
#include "register.h"
#include "stdint.h"
#include "tim4.h"
#include "clock.h"
#include "stdbool.h"
#include "driver.h"
//#include "ADC.h"

//uint16_t adcval = 0;
//uint16_t getADC1(uint8_t ch)
//{
//U2send_string("ADCT15");
//	adcval=0XFFFF;  		 
//	ADC1_SQR3&=0XFFFFFFE0;
//	ADC1_SQR3|=ch;
//	U2send_string("ADCT17");
//	ADC1_CR2 |= (1<<22);
//	U2send_string("ADCT16");
//	for(int i=0;i<=100;i++);
//	while(adcval==0XFFFF);	  	 	   
//	return adcval;		
//}

//void ADC1_2_IRQHandler(void)
//{
//	U2send_string("ADCT111");
//		adcval=ADC1_DR;
//		ADC1_SR&=~(1<<1);
//}

typedef struct {
		uint8_t type;
		int adc;
} cmd;

char aa[100];
char UI[20];
char tUI[20];
char comment[20]="wrong command";
bool inputstr(char *const buf, int const bufsize);
//int high_table[12]={3000,2650,1700,1200,900,750,650,550,500,450,375};
int high_table[12]={2790,2600,2300,1700,1200,880,750,590,500,450,400,320};
unsigned const int up =1;
unsigned const int down =2;
unsigned const int begin =3;
unsigned const int finish =4;

int calhight(int height)
{
		int setpoint = 0;
		int n = height /5;
		int rel = height %5;
		setpoint = high_table[n-1];
		setpoint +=(int)((high_table[n]-high_table[n-1])*(rel/5.0));
		return setpoint;
}

void A2D_INIT(){

	RCC_APB2ENR|=1<<9;
	RCC_APB2ENR|=1<<4;
	GPIOC_CRL&=0XFFFFFFF0;
	ADC1_SMPR1&=~(7<<0); //set 55.5 cycle on    
 	ADC1_SMPR1|=5<<0; 
	ADC1_CR2|=1<<0;	    
	ADC1_CR2|=1<<3;         
	//while(ADC1_CR2&1<<3); 			 
	ADC1_CR2|=1<<2;          
	while(ADC1_CR2&1<<2){}; 
}

void A2D_START_channel(uint8_t channel){
	ADC1_SQR3|=channel;
	ADC1_CR2|=1<<0;
}

uint32_t A2D_READ(){
	uint32_t temp;
	while((ADC1_SR & 0x2) == 0){}
	temp = ADC1_DR;
	return temp;
}



int main(void)
{
		//initial STM32 system clock-72MHz
		Stm32_Clock_Init(9);
	
		//inital timer4 use to control the servo motor
		time4_pwm_ini();
		
		//initial USART2
		USART2_ini();
		A2D_INIT();
		//ADC1_Init();
		//A2D_INIT();
    U2send_string("CMD: ");
	
    /* Create the queue. */
    xPlant = xQueueCreate( mainQUEUE_LENGTH, sizeof( unsigned int ) ); //1 start 2 finish 3 up 4 down
		xControlin = xQueueCreate( mainQUEUE_LENGTH+1, sizeof(cmd));
    if( xPlant != NULL )
    {
        /* Start the two tasks as described in the accompanying application
        note. */
//         xTaskCreate( prvQueueReceiveTask, ( signed char * ) "Rx",
//                  configMINIMAL_STACK_SIZE, NULL,
//                  mainQUEUE_RECEIVE_TASK_PRIORITY, NULL );
//         xTaskCreate( prvQueueSendTask, ( signed char * ) "TX",
//                  configMINIMAL_STACK_SIZE, NULL,
//                  mainQUEUE_SEND_TASK_PRIORITY, NULL );

//        xTaskCreate( task0, (signed char *) "TASK0",
//                 configMINIMAL_STACK_SIZE, NULL,
//                 mainTASK0_PRIORITY, NULL );
//        xTaskCreate( task1, (signed char *) "TASK1",
//                 configMINIMAL_STACK_SIZE, NULL,
//                 mainTASK1_PRIORITY, NULL );
        /* Start the tasks running. */
			
			xTaskCreate( simPlantTask, (signed char *) "simPlantTask",
                 configMINIMAL_STACK_SIZE, NULL,
                 plant_Priority, NULL );
			xTaskCreate( UITask, (signed char *) "UITask",
                 configMINIMAL_STACK_SIZE, NULL,
                 IU_Priority, NULL );
			xTaskCreate( ControlTask, (signed char *) "ControlTask",
                 configMINIMAL_STACK_SIZE, NULL,
                 control_Priority, NULL );
			xTaskCreate( ADCTask, (signed char *) "ADCTask",
                 configMINIMAL_STACK_SIZE, NULL,
                 ADC_Priority, NULL );
        vTaskStartScheduler();
    }

    /* If all is well we will never reach here as the scheduler will now be
    running.  If we do reach here then it is likely that there was insufficient
    heap available for the idle task to be created. */
    for( ;; )
        ;
}
/*-----------------------------------------------------------*/

//static void task0( void *pvParameters )
//{
//    portTickType xNextWakeTime = xTaskGetTickCount();
//    
//    while (1) {
//				U2send_string("UI while");
//        GPIOB_BSRR = 1u<<8; // set bit 11
//        for (int i=0; i < 512000; ++i)
//        {} // work
//        GPIOB_BSRR = 1u<<24; // clr bit 11
//        vTaskDelayUntil(&xNextWakeTime, 25);
//    }

//}
//static void task1( void *pvParameters )
//{
//    portTickType xNextWakeTime = xTaskGetTickCount();
//    
//    while (1) {
//        GPIOB_BSRR = 1u<<12; // set bit 12
//        for (int i=0; i < 60000; ++i)
//        {}
//        GPIOB_BSRR = 1u<<28; // clr bit 12
//        vTaskDelayUntil(&xNextWakeTime, 100);
//    }

//}

/*---------------------------------------------------------------*/
/*Lab 7 function implementation*/

static void simPlantTask(void *pvParameters)
{
		portTickType xNextWakeTime = xTaskGetTickCount();
		int fun;
	  
		while (1) {
//			U2send_string("up11");
//					U2send_newline();
				/*revceive command from queue xplant and use 
					command value to determine the direction of
					the servo */ 
				if(xQueueReceive( xPlant, &fun, 0 ))
				{	
					if(fun==1)
					{
							duty_set(1250);  
						//U2send_string("up");
						//	U2send_newline();
					}						
					if(fun==2)
					{						
							duty_set(1160);
						//U2send_string("down");
						//U2send_newline();
					}
					if(fun==3)
					{
						duty_set(1250);
					//	U2send_string("open");
						//U2send_newline();
					}
					if(fun==4)
					{
						duty_set(1000);
						//U2send_string("close");
						//U2send_newline();
					}
				}
				vTaskDelayUntil(&xNextWakeTime, 5);
		}
}	

static void UITask(void *pvParameters)
{
		portTickType xNextWakeTime = xTaskGetTickCount();
		while(1){
				if(inputstr(aa,100)){
							cmd ex;
							ex.type = 1; //1 means key board input
							xQueueSend(xControlin, &ex,0);
							//U2send_string("test: UITask xControlin finish");
							//U2send_newline();
				}
				vTaskDelayUntil(&xNextWakeTime, 40);
		}
}
char buff[20];
static void ControlTask(void *pvParameters)
{
		portTickType xNextWakeTime = xTaskGetTickCount();
		cmd ReceiveValue;
		int nheight;
		static int setpoint = 0;
		static bool start=false;
		while(1){
				//U2send_string("aaaa");
				//f finish b begin
				if(xQueueReceive( xControlin, &ReceiveValue, 0 ))
				{	
							//U2send_string("controlTask");
							//U2send_newline();
							if(ReceiveValue.type==1){
									int n;
									n=sscanf(aa,"%s %s",UI,tUI);
									if (n==1){
												n = sscanf(aa,"%d%s",&nheight,tUI);
												if(n>=2)
												{
														U2send_string(comment);
														U2send_newline();
														U2send_string("cmd: ");
												
												}
												else if(nheight>10&&nheight<60){
														setpoint=calhight(nheight);
//													sprintf(buff,"%d",setpoint);			
//													U2send_string(buff);
//													U2send_newline();
													U2send_string("cmd: ");
								
												}//
												else if(nheight==60)
												{
													xQueueSend(xPlant, &begin,0);
													start=true;
													U2send_string("cmd: ");
						
												}
												else if(nheight==1)
												{
													xQueueSend(xPlant, &finish,0);
													start=false;
													U2send_string("cmd: ");
												
												}
												else
												{
													U2send_string("wrong com");
													U2send_newline();
													U2send_string("cmd: ");
													//U2send_newline();
												}
									}
									else if (n>1){
											U2send_string(comment);
											U2send_newline();
										  U2send_string("cmd: ");
												//	U2send_newline();
									}
							}
							if(ReceiveValue.type==2 && start)
							{
								if(ReceiveValue.adc < setpoint)
										xQueueSend(xPlant, &down,0);
								if(ReceiveValue.adc > setpoint)
										xQueueSend(xPlant, &up,0);
							}
				}
				vTaskDelayUntil(&xNextWakeTime, 20);
		}
}

static void ADCTask(void *pvParameters)
{
	char buff[20];
		portTickType xNextWakeTime = xTaskGetTickCount();
		while(1){
			//U2send_string("ADCT11");
			//U2send_newline();
			A2D_START_channel(10);
				cmd ex;
				ex.type =2;
				ex.adc = A2D_READ();
				//sprintf(buff,"%d",ex.adc);			
				//U2send_string(buff);
		//	U2send_newline();
				xQueueSend(xControlin, &ex,0);
			
				vTaskDelayUntil(&xNextWakeTime, 20);
		}
}

//static void prvQueueSendTask( void *pvParameters )
//{
//portTickType xNextWakeTime;
//const unsigned long ulValueToSend = 100UL;

//    /* Initialise xNextWakeTime - this only needs to be done once. */
//    xNextWakeTime = xTaskGetTickCount();

//    for( ;; )
//    {
//        /* Place this task in the blocked state until it is time to run again.
//        The block time is specified in ticks, the constant used converts ticks
//        to ms.  While in the Blocked state this task will not consume any CPU
//        time. */
//        vTaskDelayUntil( &xNextWakeTime, mainQUEUE_SEND_FREQUENCY_MS );

//        /* Send to the queue - causing the queue receive task to unblock and
//        print out a message.  0 is used as the block time so the sending 
//        operation will not block - it shouldn't need to block as the queue 
//        should always be empty at this point in the code. */
//        xQueueSend( xQueue, &ulValueToSend, 0 );
//    }
//}
/*-----------------------------------------------------------*/

//static void prvQueueReceiveTask( void *pvParameters )
//{
//unsigned long ulReceivedValue;

//    for( ;; )
//    {
//        /* Wait until something arrives in the queue - this task will block
//        indefinitely provided INCLUDE_vTaskSuspend is set to 1 in
//        FreeRTOSConfig.h. */
//        xQueueReceive( xQueue, &ulReceivedValue, portMAX_DELAY );

//        /*  To get here something must have been received from the queue, but
//        is it the expected value?  If it is, print out a pass message, if no,
//        print out a fail message. */
//        if( ulReceivedValue == 100UL )
//        {
//            printf( "Value 100 received - Tick = 0x%x\r\n", xTaskGetTickCount() );
//        }
//        else
//        {
//            printf( "Received an unexpected value\r\n" );
//        }
//    }
//}
/*-----------------------------------------------------------*/

int fputc( int iChar, FILE *pxNotUsed ) 
{
    /* Just to avoid compiler warnings. */
    ( void ) pxNotUsed;

    if( mainDEMCR & mainTRCENA ) 
    {
        while( mainITM_Port32( 0 ) == 0 );
        mainITM_Port8( 0 ) = iChar;
    }

    return( iChar );
}

bool inputstr(char *const buf, int const bufsize)
{
		static int pos = 0;
		if(kbhit()){
			char input = U2get();
			switch(input){
				case '\b':
						if(pos>0){
								pos--;
								U2send_string("\b \b");
						}
						break;
				case '\r':
						buf[pos]='\0';
						pos=0;
						U2send_newline();
						return true;
				default:
						if(pos==bufsize-1)
							buf[pos]='\0';
						else{
								U2send(input);
								buf[pos++]=input;
						}
						break;
			}
  	}
		return false;
}
