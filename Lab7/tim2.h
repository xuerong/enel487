#ifndef TIM2_H
#define TIM2_H

#include "stdint.h"

void time_sysclock(void);
void time_init(uint16_t ARR,uint16_t PSC);
uint16_t timer_start(void);
uint16_t timer_stop(uint16_t start_time);
void timer_shutdown(void);
void time_init_with_interrupt(uint16_t ARR,uint16_t PSC);

double test_rand_32_add(void);
double test_rand_64_add(void);
double test_rand_32_mul(void);
double test_rand_64_mul(void);
double test_rand_32_div(void);
double test_rand_64_div(void);
double test_struct_8_copy(void);
double test_struct_128_copy(void);
double test_struct_1024_copy(void);

long rand_32(void);
long long int rand_64(void);
#endif
