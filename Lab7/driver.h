#ifndef DRIVER_H 
#define DRIVER_H
#include "stdint.h"
#include "register.h"	
#include "stdbool.h"
void USART2_ini(void);
void USART2_close(void);
void USART_sysclock(void);
void LEDS_ini(void);
void LED0_toggle(void);
void LED_on(int i);//from led0 to led7
void LED_off(int i);//from led0 to led7
void LEDs_on(void);
void LEDs_off(void);
void U2send(char a);
void U2send_newline(void);
char U2get(void);
void U2send_string(char a[]);
int isLed_on(int i);
void USART1_ini(void);
void U1send(char a);
char U1get(void);
bool kbhit(void);
//void U2send_string_point(char *a,int number)
#endif
