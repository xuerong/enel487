Project Name: Complex Number Calculator
Author : Xuerong Wu
Files : main.c Complex_number.cpp Complex_number.h
Description : This project is used to caluculate the sum, substraction, multiplication, and 
              divistion of two complex number. Also, can bye use to calculate the absolute value, 
              angle, exponential, and inverse of a complex number.
              The program supports two input modes: 
              interactive and batch mode.
Complier : gcc 4.9.2
Environment : Ununtu 4.9.2

Performance of the Program : User select the complex number algorithm by typing (a, s, m, d,abs, arg, argDeg,exp,inv) for
                             addition, substraction, multiplication,division, absolute, agle, exponential, inverse. (capital letter also work).
                             If the complex numbers user entered can not do division, the system will output a
                             warning line ("The complex numbers entered can not do division").
                             If the algorithm user entered do not exist, the system will output "illegal math operation involved".
                             example: a 1 1 1 1
                                      s 1 1 1 1
                                      m 1 1 1 1
                                      d 1 1 1 1
                                      abs 1 1 
                                      arg 1 1 
                                      argDeg 1 1
                                      exp 1 1
                                      inv 1 1
                                      q
                             The program is not case sensitive. However, any invalid input will be examined.
                             Batch mode: ./executable_file_name <input_file_name> output_file_name
			     Interactive mode: ./executable_file_name
