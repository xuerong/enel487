/******************************************************************************************************
File : complex_number.cpp
Description : The file declares function implementation
*******************************************************************************************************/
#include "complex_number.h"


// Next 4 functions implement add, subtract, multiply, divide for complex
struct Complex_number complex_number_add(struct Complex_number a, struct Complex_number b)
{
    struct Complex_number c;
    c.real = a.real + b.real;
    c.image = a.image + b.image;
    return c;
}
struct Complex_number complex_number_sub(struct Complex_number a, struct Complex_number b)
{
    struct Complex_number c;
    c.real = a.real - b.real;
    c.image = a.image - b.image;
    return c;
}
struct Complex_number complex_number_mul(struct Complex_number a, struct Complex_number b)
{
    struct Complex_number c;
    c.real = a.real*b.real - a.image*b.image;
    c.image = a.real*b.image + a.image*b.real;
    return c;
}
struct Complex_number complex_number_div(struct Complex_number a, struct Complex_number b)
{
    struct Complex_number c;
    double denom = b.real*b.real + b.image*b.image;
    if(fabs(denom) - 0 < 1e-20)
        fprintf(stderr, "Dividion by 0: input line was: D %lg %lg %lg %lg\n",a.real,a.image,b.real,b.image);
        c.real = (a.real*b.real + a.image*b.image) / denom;
        c.image = (a.image*b.real - a.real*b.image) / denom;
        return c;
}

/**
 Purpose: the function is used to caluculate absolute value of a complex number
 return absolute value of a complex number in double
 @param: a Complex_number
 */
double complex_number_abs(struct Complex_number a)
{
    return sqrt(a.image*a.image+a.real*a.real);
}
/**
 Purpose: the function is used to caluculate angle of a complex number
 return double in radians
 @param: a Complex_number
 */
double complex_number_arg(struct Complex_number a)
{
    double temp = fabs(atan(a.image/a.real));
    
    if (a.image>0 && a.real>0)
    {
        return temp;
    }
    else if (a.image>0 && a.real<0)
    {
        return M_PI-temp;
    }
    else if (a.image<0 && a.real<0)
    {
        return temp-M_PI;
    }
    else if (a.image<0 && a.real>0)
    {
        return -temp;
    }
    else if (a.image<0)
    {
        return -temp;
    }
    else if (a.real<0)
    {
        return M_PI-temp;
    }
    return temp;
}
/**
 Purpose: the function is used to caluculate angle of a complex number
 return double in degree
 @param: a Complex_number
 */
double complex_number_argDeg(struct Complex_number a)
{
    double temp = fabs(atan(a.image/a.real));
    
    if (a.image>0 && a.real>0)
    {
        return rad_to_deg(temp);
    }
    else if (a.image>0 && a.real<0)
    {
        return rad_to_deg(M_PI-temp);
    }
    else if (a.image<0 && a.real<0)
    {
        return rad_to_deg(temp-M_PI);
    }
    else if (a.image<0 && a.real>0)
    {
        return rad_to_deg(-temp);
    }
    else if (a.image<0)
    {
        return rad_to_deg(-temp);
    }
    else if (a.real<0)
    {
        return rad_to_deg(M_PI-temp);
    }
    return rad_to_deg(temp);
}
/**
 Purpose: convert radius to degree
 return double in degree
 @param: a double
 */
double rad_to_deg(double a)
{
    return (a/(2*M_PI))*360;
}
/**
 Purpose: calculate exponential of a complex number
 return a Complex number
 @param: a Complex number
 */
struct Complex_number complex_number_exp(struct Complex_number a)
{
    struct Complex_number temp;
    temp.real = exp(a.real)*cos(a.image);
    temp.image = exp(a.real)*sin(a.image);
    return temp;
}
/**
 Purpose: calculate inverse of a complex number
 return a Complex number
 @param: a Complex number
 */
struct Complex_number complex_number_inv(struct Complex_number a)
{
    struct Complex_number temp;
    temp.real = a.real/(a.real*a.real+a.image*a.image);
    temp.image = (-a.image)/(a.real*a.real+a.image*a.image);
    return temp;
}
