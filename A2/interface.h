/******************************************************************************************************
File : interface.h
Description : This .h file declare a struct called Complex_number, and function prototypes
*******************************************************************************************************/
#ifndef INTERFACE_H
#define INTERFACE_H
#include "stdbool.h"
#include "complex_number.h"
#include "assert.h"
#include "string.h"
#include "ctype.h"

#define length 100

void introduction(void);

char* rmspace(char line[]);
void format_string(char line[]);
bool isquit(char line[]);
bool isbreakline(char line[]);
void calculation(char line[]);
bool iscommand(char line[]);
bool isnumberstring(char str[]);
void cprint(struct Complex_number a);
void toCapital(char line[]);

struct Complex_number basic_operation(char cmd,struct Complex_number a,struct Complex_number b);

#endif
