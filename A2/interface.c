/******************************************************************************************************
File : interface.c
Description : The file declares function implementation
*******************************************************************************************************/
#include "interface.h"

//return a char point to first non-white space
char* rmspace(char line[])
{
    char* p = line;
    for (; *p != '\0'; p++)
    {
        if (!(*p == ' ' || *p == '\t' || *p == '\n') )
        {
            break;
        }
    }
    return p;
}
// Recognize blank lines;
bool isbreakline(char line[])
{
    char* p=rmspace(line);
    if (*p == '\0') {
        return true;
    }
    else
    {
        return false;
    }
}

// Print introduction of the complex calculator
void introduction()
{
    fprintf(stderr,"Complex calculator\n\n");
    fprintf(stderr,
            " Type a letter to specify the arithmetic operator (A, S, M, D)\n"
            " followed by two complex numbers expressed as pairs of doubles.\n"
            " Type abs, arg, argDeg, exp, and inv\n"
            " followed by a complex numbers expressed as pairs of doubles.\n"
            " Type Q to quit.\n\n");
}

// Check for quit
bool isquit(char line[])
{
    format_string(line);
    if (strlen(line)==1&&toupper(*line)=='Q') {
        return true;
    }
    return false;
}

//eliminate extra space
void format_string(char line[])
{
    char temp[length]="";
    //if(isbreakline(line)==true)
    {
    char* p=rmspace(line);
    int j = 0;
    for (; *p!='\0'; p++)
    {
        if (*p == ' ')
        {
            if (temp[j-1]==' ' || temp[j-1] == '\n')
            {
                continue;
            }
            else
            {
                temp[j] = *p;
                j++;
            }
            
        }
        else if(*p == '\t')
        {
            if (temp[j-1]==' ' || temp[j-1] == '\n')
            {
                continue;
            }
            else
            {
                temp[j] = ' ';
                j++;
            }
        }
        else
        {
            temp[j] = *p;
            j++;
        }
    }
    if (strlen(temp)!=0)
    {
    for (int i = (int)(strlen(temp))-1; i>=0; i--) {
        if (temp[i]==' '|| temp[i] == '\t' || temp[i] == '\n') {
            continue;
        }
        else
        {
            temp[i+1]='\0';
            break;
        }
    }
    }
    }
    strcpy(line, temp);
}

//check if it is a correct input
bool iscommand(char line[])
{
    char temp[length]="";
    strcpy(temp,line);
    format_string(temp);
    int tnum;
    char s1[length] = "";
    char s2[length] = "";
    char s3[length] = "";
    char s4[length] = "";
    char s5[length] = "";
    char s6[length] = "";
    tnum = sscanf(line,"%s %s %s %s %s %s", s1,s2,s3,s4,s5,s6);
    if (tnum==3||tnum==5) {
        if (!(isnumberstring(s2)&&isnumberstring(s3)&&isnumberstring(s4)&&isnumberstring(s5)))
        {
            //fprintf(stderr, "combine wrong\n");
            return false;
        }
        struct Complex_number a;
        struct Complex_number b;
        char cm;
        char cmd[10];
        int num;
        num = sscanf(temp, "%c %lg %lg %lg %lg",&cm,&a.real,&a.image,&b.real,&b.image);
        if (num == 5 && tnum==5) {
            return true;
        }
        num = sscanf(temp, "%s %lg %lg",cmd,&a.real,&a.image);
        if (num == 3 && tnum==3 ) {
            
            return true;
        }
        return false;
    }
    else
    {
        //fprintf(stderr, "Malformed command: input line was: %s\n",line);
        return false;
    }
   
}

//operation addition or substraction or multiplication or division by comparing char cmd
struct Complex_number basic_operation(char cmd,struct Complex_number a,struct Complex_number b)
{

    struct Complex_number result = {0,0};
    switch (toupper(cmd)) {
        case 'A':
            result = complex_number_add(a,b);
            break;
        case 'S':
            result = complex_number_sub(a,b);
            break;
        case 'M':
            result = complex_number_mul(a,b);
            break;
        case 'D':
            result = complex_number_div(a,b);
            break;
        default:
            fprintf(stderr, "Malformed command: input line was: %c %lg %lg %lg %lg\n", cmd,a.real,a.image,b.real,b.image);;
            break;
    }
    return result;
}
//make sure the string is a valid number
bool isnumberstring(char str[])
{
    format_string(str);
    int dot = 0;
    int e =0;
    int i = 0;
    for (; str[i] != '\0'; i++) {
        if ((int)str[i] <= 57 && (int)str[i] >= 48) {
            continue;
        }
        else if (str[i] == 'E'|| str[i] == 'e') {
            e++;
            if (e>=2) {
                return false;
            }
        }
        else if (str[i] == '.') {
            dot++;
            if (dot>=2) {
                return false;
            }
        }
        else if (str[i] == '-') {
            if (i==0) {
                continue;
            }
            else if( str[i-1] == 'E'|| str[i-1] == 'e') {
                continue;
            }
            else{
                return false;
            }
            
        }
        else{
            return false;
        }
        
    }
    if (str[i-1] == 'E'|| str[i-1] == 'e'||str[i-1] == '-') {
        return false;
    }
    else
        return true;
}
//operating the complex number operation by using input line
void calculation(char line[])
{
    
    if (iscommand(line)==false)
    {
        fprintf(stderr, "Malformed command: input line was1: %s\n", line);
    }
    else
    {
        struct Complex_number a;
        struct Complex_number b;
        struct Complex_number c;
        struct Complex_number result;
        double final;
        char cm;
        char cmd[10];
        int num,num1;
        num = sscanf(line, "%c %lg %lg %lg %lg",&cm,&a.real,&a.image,&b.real,&b.image);
        num1 = sscanf(line, "%s %lg %lg",cmd,&c.real,&c.image);
        if (num == 5) {
            switch (toupper(cm)) {
                case 'A':
                    result = complex_number_add(a,b);
                    cprint(result);
                    break;
                case 'S':
                    result = complex_number_sub(a,b);
                    cprint(result);
                    break;
                case 'M':
                    result = complex_number_mul(a,b);
                    cprint(result);
                    break;
                case 'D':
                    result = complex_number_div(a,b);
                    cprint(result);
                    break;
                default:
                    fprintf(stderr, "Malformed command: input line was: %s\n", line);
		    fprintf(stdout,"\n");
                    break;
            }
        }
        else if (num1 == 3) {
            
            toCapital(cmd);
            
            if (strcmp(cmd, "ABS")==0) {
                final = complex_number_abs(c);
                fprintf(stdout, "%lg\n", final);
            }
            else if (strcmp(cmd, "ARG")==0) {
                final = complex_number_arg(c);
                fprintf(stdout, "%lg\n", final);
            }
            else if (strcmp(cmd, "ARGDEG")==0) {
                final = complex_number_argDeg(c);
                fprintf(stdout, "%lg\n", final);
            }
            else if (strcmp(cmd, "EXP")==0) {
                result = complex_number_exp(c);
                cprint(result);
            }
            else if (strcmp(cmd, "INV")==0) {
                result = complex_number_inv(c);
                cprint(result);
            }
            else{
                fprintf(stderr, "Malformed command: input line was: %s\n", line);
		fprintf(stdout,"\n");

            }
        }
    }
}

// Print a complex number
void cprint(struct Complex_number a)
{
    char sign = '+';
    if (a.image < 0)
        sign = '-';
    fprintf(stdout,"%g %c j %g\n", a.real, sign, fabs(a.image));
}
//change to string to capital letter
void toCapital(char line[])
{
    for(int i = 0; line[i] !='\0';i++)
    {
        line[i] = toupper(line[i]);
    }
}
