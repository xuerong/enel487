/******************************************************************************************************
File : main.cpp
Author : Xuerong Wu
Description : This program can be used to calculate the addition, substraction, multiplication, and division 
              of two complex numbers.
	      Also can be used to calculate inverse, mexponential, angle, and absolute of a complex number.
Compiler : gcc version 4.9.2
*******************************************************************************************************/
#include "stdio.h"
#include "interface.h"
int main(int argc, const char * argv[]) {
    introduction();
    do {
        char input[length]="";
        fgets(input, length, stdin);
        if (isquit(input)) {
            break;
        }
        else if (isbreakline(input)){
            fprintf(stdout,"\n");
        }
        else
        {
            calculation(input);
        }
    }while(1);

}

