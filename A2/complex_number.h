/******************************************************************************************************
File : Complex_number.h
Description : This .h file declare a struct called Complex_number, and function prototypes
*******************************************************************************************************/
#ifndef complex_number_h
#define complex_number_h

#include <stdio.h>
#include "math.h"
#include "assert.h"

struct Complex_number
{
    double real;
    double image;
};
struct Complex_number complex_number_add(struct Complex_number a, struct Complex_number b);
struct Complex_number complex_number_sub(struct Complex_number a, struct Complex_number b);
struct Complex_number complex_number_mul(struct Complex_number a, struct Complex_number b);
struct Complex_number complex_number_div(struct Complex_number a, struct Complex_number b);
double complex_number_abs(struct Complex_number a);
double complex_number_arg(struct Complex_number a);
double complex_number_argDeg(struct Complex_number a);
struct Complex_number complex_number_exp(struct Complex_number a);
struct Complex_number complex_number_inv(struct Complex_number a);
double rad_to_deg(double a);

#endif /* complex_number_h */
