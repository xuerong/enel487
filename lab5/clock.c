#include "clock.h"
void Stm32_Clock_Init(uint8_t PLL)
{
		unsigned char temp=0;   

 	RCC_CR|=0x00010000;  
	while(!(RCC_CR>>17));
	RCC_CFGR=0X00000400; 
	PLL-=2;
	RCC_CFGR|=PLL<<18;  
	RCC_CFGR|=1<<16;	  
	FLASH_ACR|=0x32;	//importance for ADC  
	RCC_CR|=0x01000000;  
	while(!(RCC_CR>>25));
	RCC_CFGR|=0x00000002; 
	while(temp!=0x02)    
	{   
		temp=RCC_CFGR>>2;
		temp&=0x03;
	}    
}

