#include "driver.h"
#include "tim2.h"
#include "interface.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "string.h"
#include "tim4.h"
#include "ADC.h"
#include "clock.h"
#define clength 100
void lab4(void);
void lab3_phase4(void);

int main () {

	Stm32_Clock_Init(9);
	USART_sysclock();
	USART2_ini();
	LEDS_ini();
	ADC1_Init();
	uint16_t temp;
	float voltage;
	while(1)
	{
		char str[20]="";
		temp = getADC1(10);
		voltage = toDecimalV(temp);
		sprintf(str,"ADC1=%.2fV",voltage);
		U2send_string(str);
	  U2send_newline();
		for(int i=0;i<=1000000;i++);
	}

}
void lab3_phase4(void)
{
	  double a;
	  char nstring[10];
		U2send_string("---------------------------------------------------------");
	  U2send_newline();
	  U2send_string("                 Phase 2 summay                          ");
	  U2send_newline();
	  U2send_string("add two random 32-bit integers");
	  U2send_newline();
		a = test_rand_32_add();
	  //a *= 13.9;
		sprintf(nstring,"%.2lf",a);
		U2send_string(nstring);
   	//U2send_string("nS");
	  U2send_newline();
		U2send_string("add two random 64-bit integers");
	  U2send_newline();
		a = test_rand_64_add()-22;
	  a *= 13.9;
		sprintf(nstring,"%.2lf",a);
		U2send_string(nstring);
	  U2send_string("nS");
	  U2send_newline();
		U2send_string("multiply two random 32-bit integers");
	  U2send_newline();
		a = test_rand_32_mul()-22;
		a *= 13.9;
		sprintf(nstring,"%.2lf",a);
		U2send_string(nstring);
		U2send_string("nS");
	  U2send_newline();
		U2send_string("multiply two random 64-bit integers");
	  U2send_newline();
		a = test_rand_64_mul();
		a *= 13.9;
		sprintf(nstring,"%.2lf",a);
		U2send_string(nstring);
		U2send_string("nS");
	  U2send_newline();
		U2send_string("divide two random 32-bit integers");
	  U2send_newline();
		a = test_rand_32_div();
		a *= 13.9;
		sprintf(nstring,"%.2lf",a);
		U2send_string(nstring);
		U2send_string("nS");
	  U2send_newline();
		U2send_string("divide two random 64-bit integers");
	  U2send_newline();
		a = test_rand_64_div();
		a *= 13.9;
		sprintf(nstring,"%.2lf",a);
		U2send_string(nstring);
		U2send_string("nS");
	  U2send_newline();
		U2send_string("copy an 8-byte struct using the assignment operator");
	  U2send_newline();
		a = test_struct_8_copy();
		a *= 13.9;
		sprintf(nstring,"%.2lf",a);
		U2send_string(nstring);
		U2send_string("nS");
	  U2send_newline();
		U2send_string("copy an 128-byte struct using the assignment operator");
	  U2send_newline();
		a = test_struct_128_copy();
		a *= 13.9;
		sprintf(nstring,"%.2lf",a);
		U2send_string(nstring);
		U2send_string("nS");
	  U2send_newline();
		U2send_string("copy an 1024-byte struct using the assignment operator");
		U2send_newline();
		U2send_string("There is problem!!!!!");
		U2send_newline();
//	  U2send_newline();
//		a = test_struct_1024_copy();
//		a *= 13.9;
//		sprintf(nstring,"%.2lf",a);
//		U2send_string(nstring);
//		U2send_string("nS");
//	  U2send_newline();
		U2send_string("---------------------------------------------------------");
	  U2send_newline();
}

void lab4(void)
{
		while(1)
	{
		U2send_string("Percentage(from 0 to 1): ");
		char a[100];
		char temp;
		double percentage;
		int n;
		for(int i=0; i<100;i++)
		{
				temp = U2get();
				if(temp == '\n' || temp == '\r')
				{
					U2send_newline();
					a[i]='\0';
					break;
				}
				else 
				{
					U2send(temp);
					a[i]=temp;
				}
				
		}
		n = sscanf(a,"%lg",&percentage);
		if( n==1)
		{
			if(percentage>1||percentage<0)
			{
				U2send_string("Wrong range!!");
        U2send_newline();
			}
			else
			{
				time4_pwm_duty_cycle((percentage));
			  U2send_string("the angle is changed");
        U2send_newline();
			}
		}
		else
		{
			U2send_string("not a command!!!");
			U2send_newline();
		}
	}
	
}
