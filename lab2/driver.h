#ifndef DRIVER_H 
#define DRIVER_H
#include "stdint.h"

#define GPIOA_BASE ((uint32_t) 0x40010800)
#define GPIOA_CRL (*(uint32_t volatile *) (GPIOA_BASE + 0x00))
#define GPIOA_CRH (*(uint32_t volatile *) (GPIOA_BASE + 0x04))

#define GPIOB_BASE ((uint32_t) 0x40010C00)
#define GPIOB_CRH (*(uint32_t volatile *) (GPIOB_BASE + 0x04))
#define GPIOB_ODR (*(uint32_t volatile *) (GPIOB_BASE + 0x0C))	
#define GPIOB_BSRR (*(uint32_t volatile *) (GPIOB_BASE + 0x10))
#define GPIOB_BRR (*(uint32_t volatile *) (GPIOB_BASE + 0x14))

#define RCC_BASE ((uint32_t) 0x40021000)
#define RCC_CR (*(uint32_t volatile *) (RCC_BASE + 0x00))
#define RCC_CFGR (*(uint32_t volatile *) (RCC_BASE + 0x04))
#define RCC_APB2ENR (*(uint32_t volatile *) (RCC_BASE + 0x18))
#define RCC_APB1ENR (*(uint32_t volatile *) (RCC_BASE + 0x1C))
	
#define USART2_BASE ((uint32_t) 0x40004400)
#define USART2_SR (*(uint32_t volatile *) (USART2_BASE + 0x00))
#define USART2_DR (*(uint32_t volatile *) (USART2_BASE + 0x04))
#define USART2_BRR (*(uint32_t volatile *) (USART2_BASE + 0x08))
#define USART2_CR1 (*(uint32_t volatile *) (USART2_BASE + 0x0c))
#define USART_SR_TXE 0x00000080
#define USART_SR_RXNE 0x00000020

#define USART1_BASE ((uint32_t) 0x40013800)
#define USART1_SR (*(uint32_t volatile *) (USART1_BASE + 0x00))
#define USART1_DR (*(uint32_t volatile *) (USART1_BASE + 0x04))
#define USART1_BRR (*(uint32_t volatile *) (USART1_BASE + 0x08))
#define USART1_CR1 (*(uint32_t volatile *) (USART1_BASE + 0x0c))
	
void USART2_ini(void);
void USART2_close(void);
void USART_sysclock(void);
void LEDS_ini(void);
void LED_on(int i);//from led0 to led7
void LED_off(int i);//from led0 to led7
void LEDs_on(void);
void LEDs_off(void);
void U2send(char a);
char U2get(void);
void send_string(char a[]);
int isLed_on(int i);
void USART1_ini(void);
void U1send(char a);
char U1get(void);
#endif
