#include "interface.h"
#include "string.h"
#include "stdio.h"
char sLED_ON[] = "The LED is on.\n\r";
char sLED_OFF[] = "The LED is off.\n\r";
char sHELP[] = "Please type: help(to check correct commands)\n\r";
char Guide[] = "turn on led n(n form 0 to 7) \n\rturn off led n(n form 0 to 7) \n\rturn on all \n\rturn off all \n\rquey led n: check the state of nth led(n from 0 to 7) \n\rprint date and time\n\r";
void command(char cmd[])
{
    char a[20];
    char b[20];
    char c[20];
		char e[20];
    int d;
	
    sscanf(cmd,"%s %s %s %d", a,b,c,&d);
 
    if (strcmp(a, "turn")==0)
    {
        sscanf(cmd,"%s %s %s %d", a,b,c,&d);
        if (strcmp(b, "on")==0)
        {   
            if (strcmp(c, "all")==0) {
                LEDs_on();
            }
            else if (strcmp(c, "led")==0)
            {
                if (d>=0&&d<=7)
                {
                   LED_on(d);
                }
            }
        }
        else if (strcmp(b, "off")==0)
        {
            if (strcmp(c, "all")==0) {
               LEDs_off();
            }
            else if (strcmp(c, "led")==0)
            {
                if (d>=0&&d<=7)
                {
                   LED_off(d);
                }
            }
        }
    }
    else if (strcmp(a, "quey")==0)
    {
        sscanf(cmd,"%s %s %d", a,b,&d);
        if (strcmp(b, "led")==0)
        {
                if (d>=0&&d<=7)
                {
                    if(isLed_on(d)==1)
											send_string(sLED_ON);
										else if(isLed_on(d)==0)
											send_string(sLED_OFF);
                }
        }
    }
    else if (strcmp(a, "help")==0)
    {
       send_string(Guide);
    }
		else if (strcmp(a, "print")==0)
		{
				sscanf(cmd,"%s %s %s %s", a,b,c,e);
				if(strcmp(b, "date")==0)
				{
						if(strcmp(c, "and")==0)
						{
								if(strcmp(e, "time")==0)
								{
									char ma[100]="";
									char date[15]=__DATE__;
									char time[15]=__TIME__;
									strcat(ma, "Compiler time: ");
									strcat(ma, date);
									strcat(ma, " ");
									strcat(ma, time);
									strcat(ma, "\n\r");
									send_string(ma);
								}
						}
				}
		}
		else
		{
				send_string(sHELP);
		}

}
