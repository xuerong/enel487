#include "driver.h"

void USART2_ini(void)
{
	
		RCC_APB2ENR |= 0x00000001; //enable AFIO
		RCC_APB2ENR |= 0x00000004; //enable GPIOA
		RCC_APB1ENR |= 0x00020000; //enable USART2
	  GPIOA_CRL &= 0xffff00ff;
	  GPIOA_CRL |= 0x00000B00; //set PA2(TX) to output 50Mhz mode, and AF_PP
	  GPIOA_CRL |= 0x00004000; //set PA3(RX) to floating input
		USART2_BRR = 0xEA6; //set Baud rate to 9600
		USART2_CR1 |= 0x0000000C; //enable TE and RE
	  USART2_CR1 |= 0x00002000; //enable USART2

}
void USART2_close(void)
{
		RCC_APB1ENR &= ~0x00020000; //reset USART2
		
		GPIOA_CRL |= 0x00000400; //reset PA2(TX) to output 50Mhz mode, and AF_PP
	  GPIOA_CRL |= 0x00004000; //reset PA3(RX) to floating input
		
	  USART2_BRR = 0x0; //clean Baud rate 
		USART2_CR1 &= ~0x0000000c; //disable TE and RE
	  USART2_CR1 &= ~0x00002000; //disable USART
}
void USART_sysclock(void)
{
		int PLLMUL;
		RCC_CR |= 0x00010000; //enable HSE
	  while(!((RCC_CR>>17)&&0x1));//wait untill HSE stable
		RCC_CFGR = 0x00000400; //APB1=DIV2;APB2=DIV1;AHB=DIV1;
	  PLLMUL = 9 - 2;
	  RCC_CFGR |= PLLMUL<<18; //set PLLMUL
		RCC_CFGR |= 1<<16;	  //PLLSRC ON 
		RCC_CR|=0x01000000;  //PLLON
		while(!((RCC_CR>>25)&0x1)); //wait untill PLL stable
		RCC_CFGR|=0x00000002; //select PLL as system clock
		while(((RCC_CFGR>>2)&0x3) != 0x2); //wait untill system clock set 
}	
void LEDS_ini(void)
{
		RCC_APB2ENR |= 0x00000008; //enable GPIOB
		GPIOB_CRH = 0x33333333; //50MHz  General Purpose output push-pull
		GPIOB_ODR  &= ~0x0000FF00; // switch off all LEDs 
}
void LED_on(int i)
{
		GPIOB_ODR |= (0x1<<(i+8)); //turn on ith led
}
void LED_off(int i)
{
		GPIOB_ODR &= ~(1<<(i+8)); //turn off ith led
}

void LEDs_on(void)
{
		GPIOB_ODR |= 0x0000FF00; //turn on all leds
}

void LEDs_off(void)
{
		GPIOB_ODR &= ~0x0000FF00; //turn off all leds
}

void U2send(char a)
{
		while (!(USART2_SR & USART_SR_TXE)); //wait until data is transmit to the shift register
		USART2_DR = (uint8_t)a;
}

char U2get(void)
{
		while(!(USART2_SR & USART_SR_RXNE));
		return (char)(USART2_DR & 0xff);
}

void send_string(char a[])
{
	int i;
	for(i=0;a[i]!='\0';i++)
	U2send(a[i]);
}
/**
	return 1 means on
	return 0 means off
*/
int isLed_on(int i)
{
		if((GPIOB_ODR>>(i+8)&0x1)== 0x1)
		{
				return 1;
		}
		else
		{
				return 0;
		}
}

void USART1_ini(void)
{
	
	RCC_APB2ENR |= 0x00000001; //enable AFIO
	RCC_APB2ENR |= 0x00000004; //enable GPIOA
	RCC_APB2ENR |= 0x00004000; //enable USART1
	
	GPIOA_CRH &= 0xfffff00f;
	GPIOA_CRH |= 0x000004B0;
	
	USART1_BRR = 0x1D5C; //set Baud rate to 9600
	USART1_CR1 |= 0x0000000C; //enable TE and RE
	USART1_CR1 |= 0x00002000; //enable USART1

}

void U1send(char a)
{
		while (!(USART1_SR & USART_SR_TXE)); //wait until data is transmit to the shift register
		USART1_DR = (uint8_t)a;
}

char U1get(void)
{
		while(!(USART1_SR & USART_SR_RXNE));
		return (char)(USART1_DR & 0xff);
}
