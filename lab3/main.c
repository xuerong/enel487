#include "driver.h"
#include "tim2.h"
#include "interface.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#define clength 100

void TIM2_IRQHandler() 
{
	if(TIM2_SR & 0x0001) 
	{ 
		LED0_toggle();
	}
	TIM2_SR &= ~(1<<0);
}
void lab3_phase4(void);
int main () {

	srand(43);
	time_sysclock();
	USART_sysclock();
	
	LEDS_ini();
	USART2_ini();
	time_init_with_interrupt(1199,59999);
	while(1){};
   //time_init(0xFFFF,0);
	

  	//lab3_phase4();
	
}
void lab3_phase4(void)
{
	  double a;
	  char nstring[10];
		U2send_string("---------------------------------------------------------");
	  U2send_newline();
	  U2send_string("                 Phase 2 summay                          ");
	  U2send_newline();
	  U2send_string("add two random 32-bit integers");
	  U2send_newline();
		a = test_rand_32_add()-22;
	  a *= 13.9;
		sprintf(nstring,"%.2lf",a);
		U2send_string(nstring);
   	U2send_string("nS");
	  U2send_newline();
		U2send_string("add two random 64-bit integers");
	  U2send_newline();
		a = test_rand_64_add()-22;
	  a *= 13.9;
		sprintf(nstring,"%.2lf",a);
		U2send_string(nstring);
	  U2send_string("nS");
	  U2send_newline();
		U2send_string("multiply two random 32-bit integers");
	  U2send_newline();
		a = test_rand_32_mul()-22;
		a *= 13.9;
		sprintf(nstring,"%.2lf",a);
		U2send_string(nstring);
		U2send_string("nS");
	  U2send_newline();
		U2send_string("multiply two random 64-bit integers");
	  U2send_newline();
		a = test_rand_64_mul();
		a *= 13.9;
		sprintf(nstring,"%.2lf",a);
		U2send_string(nstring);
		U2send_string("nS");
	  U2send_newline();
		U2send_string("divide two random 32-bit integers");
	  U2send_newline();
		a = test_rand_32_div();
		a *= 13.9;
		sprintf(nstring,"%.2lf",a);
		U2send_string(nstring);
		U2send_string("nS");
	  U2send_newline();
		U2send_string("divide two random 64-bit integers");
	  U2send_newline();
		a = test_rand_64_div();
		a *= 13.9;
		sprintf(nstring,"%.2lf",a);
		U2send_string(nstring);
		U2send_string("nS");
	  U2send_newline();
		U2send_string("copy an 8-byte struct using the assignment operator");
	  U2send_newline();
		a = test_struct_8_copy();
		a *= 13.9;
		sprintf(nstring,"%.2lf",a);
		U2send_string(nstring);
		U2send_string("nS");
	  U2send_newline();
		U2send_string("copy an 128-byte struct using the assignment operator");
	  U2send_newline();
		a = test_struct_128_copy();
		a *= 13.9;
		sprintf(nstring,"%.2lf",a);
		U2send_string(nstring);
		U2send_string("nS");
	  U2send_newline();
		U2send_string("copy an 1024-byte struct using the assignment operator");
		U2send_newline();
		U2send_string("There is problem!!!!!");
		U2send_newline();
//	  U2send_newline();
//		a = test_struct_1024_copy();
//		a *= 13.9;
//		sprintf(nstring,"%.2lf",a);
//		U2send_string(nstring);
//		U2send_string("nS");
//	  U2send_newline();
		U2send_string("---------------------------------------------------------");
	  U2send_newline();
}
