#include "tim2.h"
#include "stdint.h"
#include "register.h"
#include "stdlib.h"
void time_init(uint16_t ARR,uint16_t PSC)
{
		RCC_APB1ENR |= 1<<0; //enable TIM2 in RCC
	
		TIM2_ARR = ARR;
	  TIM2_PSC = PSC;
		
		TIM2_CR1 |= (1 << 4); //set to countdown
		
	  TIM2_CR1 |= (1 << 0); //counter enable
	  
}

uint16_t timer_start(void)
{
		return TIM2_CNT;
}
uint16_t timer_stop(uint16_t start_time)
{
		uint16_t temp;
		temp = TIM2_CNT;
		if(start_time < temp)  // wrap around
		{
			return (start_time-0)+(0xffff-temp+1);
		}
		else
		{
				return start_time - temp;
		}
}
void timer_shutdown(void)
{
		RCC_APB1ENR &= ~(1<<0); //enable TIM2 in RCC
	  TIM2_ARR = 0;
	  TIM2_PSC = 0;
	  TIM2_CR1 &= ~(1 << 4); //set to countdown
		TIM2_DIER &= ~(0x01<<0); //enable interrupt
	  TIM2_CR1 &= ~(1 << 0); //counter enable
	  NVIC_ISER0 &= ~(1<<28);
	  
}
void time_init_with_interrupt(uint16_t ARR,uint16_t PSC)
{
	  RCC_APB1ENR |= 1<<0; //enable TIM2 in RCC
	  TIM2_ARR = ARR;
	  TIM2_PSC = PSC;
		TIM2_CR1 |= (1 << 4); //set to countdown
		TIM2_DIER |= 0x01<<0; //enable interrupt
	  TIM2_CR1 |= (1 << 0); //counter enable
	  NVIC_ISER0 |= (1<<28);
}

void time_sysclock(void)
{
		int PLLMUL;
		RCC_CR |= 0x00010000; //enable HSE
	  while(!((RCC_CR>>17)&&0x1));//wait untill HSE stable
		RCC_CFGR = 0x00000400; //APB1=DIV2;APB2=DIV1;AHB=DIV1;
	  PLLMUL = 9 - 2;
	  RCC_CFGR |= PLLMUL<<18; //set PLLMUL
		RCC_CFGR |= 1<<16;	  //PLLSRC ON 
		RCC_CR|=0x01000000;  //PLLON
		while(!((RCC_CR>>25)&0x1)); //wait untill PLL stable
		RCC_CFGR|=0x00000002; //select PLL as system clock
		while(((RCC_CFGR>>2)&0x3) != 0x2); //wait untill system clock set 
}

long rand_32(void)
{
	long rand1;
	rand1 = (rand()<<1)+rand()%2;
	return rand1;
}

long long int rand_64(void)
{
	long long int rand1;
	rand1 = ((long long int)((rand()<<1)+rand()%2) << 32)+(((uint32_t)rand()<<1)+rand()%2);
	return rand1;
}
