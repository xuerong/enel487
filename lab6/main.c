#include "driver.h"
#include "tim2.h"
#include "interface.h"
#include "stdio.h"
#include "stdlib.h"
#include "time.h"
#include "string.h"
#include "tim4.h"
#include "ADC.h"
#include "clock.h"
#include "math.h"
#define clength 100
int high_table[12]={3000,2650,1700,1200,900,750,650,550,500,450,375};
void lab4(void);
void lab3_phase4(void);
void lab5(void);
void time4_pwm_duty_cycle(double percentage)
{

	 int tmp;
	 char per[20];
	 char ini[20];
	 tmp = (int)((2400-600) * (percentage/100.0)) +600;
	 sprintf(per,"%lg",percentage);
	 U2send_string(per);
	 U2send_newline();
	sprintf(ini,"%d",tmp);
	 U2send_string(ini);
	 U2send_newline();
	 TIM4_CCR2 = (uint16_t)tmp;
}
void duty_set(int pwm)  //1230 up // 1160 down
{
		TIM4_CCR2 = (uint16_t)pwm;
}


int main () {
	int setpoint = 0;
	Stm32_Clock_Init(9);
	USART_sysclock();
	USART2_ini();
	LEDS_ini();
	ADC1_Init();
	time4_pwm_ini();
	
	while(1)
	{
		U2send_string("Ping Pong Machine 5");
		U2send_newline();
		U2send_string("Please input height(from 5cm to 60cm): ");
		char a[100];
		char temp;
		int height;
		int n;
		char b[100];
		//char str[20];
		for(int i=0; i<100;i++)
		{
				temp = U2get();
				if(temp == '\n' || temp == '\r')
				{
					U2send_newline();
					a[i]='\0';
					break;
				}
				else 
				{
					U2send(temp);
					a[i]=temp;
				}
				
		}
		n = sscanf(a,"%d%s",&height,b);
		if (n>=2)
		{
				U2send_string("Wrong command!!");
        U2send_newline();
				continue;
		}
		if(height>60||height<5)
		{
				U2send_string("Wrong range!!");
        U2send_newline();
				continue;
		}
		n = height /5;
		int rel = height %5;
		setpoint = high_table[n-1];
		setpoint +=(int)((high_table[n]-high_table[n-1])*(rel/5.0));
		duty_set(600);
		for(int i=0;i<10000000;i++);
		duty_set(1250);
		for(int i=0;i<10000000;i++);
			while(1)
		{
			int temp = getADC1(10);
			
			if(temp < setpoint)
				duty_set(1160);
			if(temp > setpoint)
				duty_set(1235);
		}
	}
}
//	
//void lab5(void)
//{
//	float voltage;
//	uint16_t temp;
//	char str[20]="";
//		temp = getADC1(10);
//		voltage = toDecimalV(temp);
//		sprintf(str,"ADC1=%d",temp);
//		U2send_string(str);
//	  U2send_newline();
//		for(int i=0;i<=10000000;i++);
//}
//void lab3_phase4(void)
//{
//	  double a;
//	  char nstring[10];
//		U2send_string("---------------------------------------------------------");
//	  U2send_newline();
//	  U2send_string("                 Phase 2 summay                          ");
//	  U2send_newline();
//	  U2send_string("add two random 32-bit integers");
//	  U2send_newline();
//		a = test_rand_32_add();
//	  //a *= 13.9;
//		sprintf(nstring,"%.2lf",a);
//		U2send_string(nstring);
//   	//U2send_string("nS");
//	  U2send_newline();
//		U2send_string("add two random 64-bit integers");
//	  U2send_newline();
//		a = test_rand_64_add()-22;
//	  a *= 13.9;
//		sprintf(nstring,"%.2lf",a);
//		U2send_string(nstring);
//	  U2send_string("nS");
//	  U2send_newline();
//		U2send_string("multiply two random 32-bit integers");
//	  U2send_newline();
//		a = test_rand_32_mul()-22;
//		a *= 13.9;
//		sprintf(nstring,"%.2lf",a);
//		U2send_string(nstring);
//		U2send_string("nS");
//	  U2send_newline();
//		U2send_string("multiply two random 64-bit integers");
//	  U2send_newline();
//		a = test_rand_64_mul();
//		a *= 13.9;
//		sprintf(nstring,"%.2lf",a);
//		U2send_string(nstring);
//		U2send_string("nS");
//	  U2send_newline();
//		U2send_string("divide two random 32-bit integers");
//	  U2send_newline();
//		a = test_rand_32_div();
//		a *= 13.9;
//		sprintf(nstring,"%.2lf",a);
//		U2send_string(nstring);
//		U2send_string("nS");
//	  U2send_newline();
//		U2send_string("divide two random 64-bit integers");
//	  U2send_newline();
//		a = test_rand_64_div();
//		a *= 13.9;
//		sprintf(nstring,"%.2lf",a);
//		U2send_string(nstring);
//		U2send_string("nS");
//	  U2send_newline();
//		U2send_string("copy an 8-byte struct using the assignment operator");
//	  U2send_newline();
//		a = test_struct_8_copy();
//		a *= 13.9;
//		sprintf(nstring,"%.2lf",a);
//		U2send_string(nstring);
//		U2send_string("nS");
//	  U2send_newline();
//		U2send_string("copy an 128-byte struct using the assignment operator");
//	  U2send_newline();
//		a = test_struct_128_copy();
//		a *= 13.9;
//		sprintf(nstring,"%.2lf",a);
//		U2send_string(nstring);
//		U2send_string("nS");
//	  U2send_newline();
//		U2send_string("copy an 1024-byte struct using the assignment operator");
//		U2send_newline();
//		U2send_string("There is problem!!!!!");
//		U2send_newline();
////	  U2send_newline();
////		a = test_struct_1024_copy();
////		a *= 13.9;
////		sprintf(nstring,"%.2lf",a);
////		U2send_string(nstring);
////		U2send_string("nS");
////	  U2send_newline();
////		U2send_string("---------------------------------------------------------");
////	  U2send_newline();
//}

//void lab4(void)
//{
//		while(1)
//	{
//		U2send_string("Percentage(from 0 to 1): ");
//		char a[100];
//		char temp;
//		double percentage;
//		int n;
//		//char str[20];
//		for(int i=0; i<100;i++)
//		{
//				temp = U2get();
//				if(temp == '\n' || temp == '\r')
//				{
//					U2send_newline();
//					a[i]='\0';
//					break;
//				}
//				else 
//				{
//					U2send(temp);
//					a[i]=temp;
//				}
//				
//		}
//		n = sscanf(a,"%lg",&percentage);
//		if( n==1)
//		{
//			if(percentage>100||percentage<0)
//			{
//				U2send_string("Wrong range!!");
//        U2send_newline();
//			}
//			else
//			{
//				time4_pwm_duty_cycle((percentage));
//			  U2send_string("the angle is changed");
//        U2send_newline();
//				for(int i=0;i<10000;i++);
//				lab5();
//			}
//		}
//		else
//		{
//			U2send_string("not a command!!!");
//			U2send_newline();
//		}
//	}
//	
//}
