#include "tim4.h"

void time4_pwm_ini(void)
{
		RCC_APB1ENR |= (1<<2); //enable timer4
		RCC_APB2ENR |= 0x01; //enable alternative function
		RCC_APB2ENR |= (1<<3); //enable GPIOB
		
		GPIOB_CRL &= 0x0FFFFFFF;  //use timer4 channel CH-2
		GPIOB_CRL |= 0xB0000000;  //configure GPIOB7
	
	  TIM4_CCER |= 1<<4; //enable signal on pin7
	  TIM4_CCER &= ~(1<<5); //set active high
	
		TIM4_CCMR1 |= 1<<14; //set to PWM1 in channel 2
	  TIM4_CCMR1 |= 1<<13;
	  TIM4_CCMR1 &= ~(1<<12);
	
		TIM4_CCMR1 |= (1<<11);//set OC2PE
	  TIM4_CCMR1 |= (1<<10);//set OC2FE
		
		TIM4_CR1 |= (1<<7);//set ARPE
	  TIM4_CR1 &= ~(1<<4);//up counting
	
	  TIM4_ARR = 19999;
	  TIM4_PSC = 71;
	  TIM4_CCR2 =600;
		

		TIM4_EGR |= 0x1;
	  TIM4_CR1 |= 0x1;//start count
	
}

//void time4_pwm_duty_cycle(double percentage)
//{

//	 int tmp;
//	 tmp = (int)((2400-600) * (percentage/100.0)) +600;
//	 TIM4_CCR2 = (uint16_t)tmp;
//}
