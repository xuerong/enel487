#ifndef ADC_H
#define ADC_H
#include "register.h"
#include "stdint.h"
void ADC1_Init(void);
void ADC1_Start_Conversion(void);
uint16_t getADC1(uint8_t ch);
float toDecimalV(uint16_t);
#endif
