/**********************************************************************
 *
 * Filename:    main.c
 * 
 * Description: A simple test program for the CRC implementations.
 *
 * Notes:       To test a different CRC standard, modify crc.h.
 *
 * 
 * Copyright (c) 2000 by Michael Barr.  This software is placed into
 * the public domain and may be used for any purpose.  However, this
 * notice must not be changed or removed and no warranty is either
 * expressed or implied by its publication or distribution.
 **********************************************************************/

#include <stdio.h>
#include <string.h>
#include "stdlib.h"
#include "string.h"
#include "crc.h"
#include "time.h"
void test_correct(void)
{
	printf("------------------------------------------------\n");
	printf("If CRC16, result should be equal to 0X778B\n");
	printf("If CRC_CCITT, result should be equal to 0X3CDE\n");
	printf("If CRC32, result should be equal to 0XDBF9C8F7\n");
	printf("Result:\n");
	crcInit();
	unsigned char a[]="1111";
	crc b;
	b = crcSlow(a,4);
	printf("The crcSlow(): 0x%X\n",b);
	b = crcFast(a,4);
	printf("The crcFast(): 0x%X\n",b);
	printf("------------------------------------------------\n");
}
void test_message_slow(unsigned char*a,int length)
{
	/*
	 * Compute the CRC of the test message, slowly.
	 */
	crcInit();
	clock_t start_t,end_t,total_t;
	crc b;
	start_t = clock();
	b=crcSlow(a, length);
	end_t = clock();
	printf("The crcSlow() of \"wrnpc11.txt\" is 0x%X\n",b);
	printf("Total time taken by CPU: %f\n",(double)(end_t-start_t)/CLOCKS_PER_SEC);
	
}
void test_message_fast(unsigned char*a,int length)
{
	/*
	 * Compute the CRC of the test message, more efficiently.
	 */
	crcInit();
	clock_t start_t,end_t,total_t;
	crc b;
	start_t = clock();
	b=crcFast(a, length);
	end_t = clock();
	printf("The crcFasti() of \"wrnpc11.txt\" is 0x%X\n",b);
	printf("Total time taken by CPU: %f\n",(double)(end_t-start_t)/CLOCKS_PER_SEC);
}
int main(void)
{
	FILE *myfile;
	size_t c;
	myfile = fopen("wrnpc11.txt","r");
	if(!myfile)
		fprintf(stderr,"The file can not open!\n");
	fseek(myfile,0,SEEK_END);
	int length = ftell(myfile);
	fseek(myfile,0,SEEK_SET);
	unsigned char *a = (unsigned char*)malloc(sizeof(char)*length);
	c=fread(a,length,sizeof(unsigned char),myfile);
	/*
	 * Print the check value for the selected CRC algorithm.
	 */
	printf("The check value for the %s standard is 0x%X\n", CRC_NAME, CHECK_VALUE);
	test_message_slow(a,length);
	test_message_fast(a,length);
	test_correct();
	return 0;
}   /* main() */
