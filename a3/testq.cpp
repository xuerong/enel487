/**
   Project: Implementation of a Queue in C++.
   Programmer: Karim Naqvi
   Course: enel487
   Description: test code
*/

#include <iostream>
#include <cassert>
#include <cstdlib>
#include <vector>

#include "queue.h"

using namespace std;

/**
   Compare the given queue to the given array of data elements.  Return
   true if they are all equal.
 */
bool testQueueEquality( Queue& q, vector<Data> const array)
{
    if (q.size() != array.size())
	return false;

    for (unsigned i = 0; i < q.size(); ++i)
    {
        Data d(0,0);
	q.remove(&d);
	if ( !d.equals(array[i]) )
	    return false;
	q.insert(d);
    }
    return true;
}
/**
   Test add an element in a queue that is empty.
*/
void testInsertEmptyQueue(void)
{
	cout<<"-----------------------------------------------"<<endl;
	cout<<"Test add an element to an empty queue"<<endl;
	Queue *myq = new Queue();
        Data d11(1,1);
	cout<<"add an element at a random position except position 0"<<endl;
	cout<<"Size of queue(Before insert): "<<myq->size()<<endl;
	myq->insert(d11,3);
	cout<<"Size of queue(After insert): "<<myq->size()<<endl;
	cout<<"add an element at 0 position"<<endl;
	cout<<"Size of queue(Before insert): "<<myq->size()<<endl;
	myq->insert(d11,0);
	cout<<"Size of queue(After insert): "<<myq->size()<<endl;
	myq->print();
	cout<<"-----------------------------------------------"<<endl;
}
/**
   Test add an element to a queue that has one element inside
*/

void testInsertOneElementQueue(void)
{
	cout<<"-----------------------------------------------"<<endl;
	cout<<"Test add an element to a queue that has one element inside"<<endl;
	Queue *q = new Queue();
	Data tmp; 
        Data d11(1,1);
        Data d33(3,3);
	q->insert(d11);
	cout<<"add an element at position 0"<<endl;
	cout<<"Print queue before insertion: ";
	q->print();
	cout<<"Size of queue(Before insert): "<<q->size()<<endl;
	q->insert(d33,0);
	cout<<"Size of queue(After insert): "<<q->size()<<endl;
	q->print();
	q->remove(&tmp);
	q->remove(&tmp);
	q->insert(d11);
	cout<<"add an element at position 1"<<endl;
	cout<<"Print queue before insertion: ";
	q->print();
	cout<<"Size of queue(Before insert): "<<q->size()<<endl;
	q->insert(d33,1);
	cout<<"Size of queue(After insert): "<<q->size()<<endl;
	q->print();
	cout<<"-----------------------------------------------"<<endl;
}
/**
   Test add an element to a queue that has more than one element inside
*/

void testInsertThreeElementQueue(void)
{
	cout<<"-----------------------------------------------"<<endl;
	cout<<"Test add an element to a queue that has more than one element inside"<<endl;
	Queue *q = new Queue();
	Data tmp; 
        Data d11(1,1);
        Data d22(2,2);
        Data d44(4,4);
        Data d33(3,3);
	q->insert(d11);
	q->insert(d22);
	q->insert(d33);
        cout<<"add an element at position 1"<<endl;
	cout<<"Print queue before insertion: ";
	q->print();
	cout<<"Size of queue(Before insert): "<<q->size()<<endl;
	q->insert(d44,1);
	cout<<"Size of queue(After insert): "<<q->size()<<endl;
	q->print();
	cout<<"-----------------------------------------------"<<endl;
}

int main(void)
{
    cout << "Testing queue.\n";
    Queue q1;

    Data d11(1,1);
    Data d33(3,3);
    Data d55(5,5);

    q1.insert(d11);
    q1.insert(d33);
    q1.insert(d55);
    q1.print();

    vector<Data> dataVec;
    dataVec.push_back(d11);
    dataVec.push_back(d33);
    dataVec.push_back(d55);

    assert(testQueueEquality(q1, dataVec));

    Data d44(4, 4);
    bool found = q1.search(d44);
    assert(found == false);


    q1.insert(d44);  // now is (1,1),(3,3),(5,5),(4,4)
    found = q1.search(d44);
    assert(found == true);

    // now queue is(1,1),(3,3),(5,5),(4,4) and 
    // dataVec has (1,1),(3,3),(5,5).  Not equal
    assert(testQueueEquality(q1, dataVec) == false);

    Data temp;
    q1.remove(&temp);  // now q1 is (3,3),(5,5),(4,4)

    Data temp2(1,1);
    assert(temp.equals(temp2));  // (1,1) == (1,1)

    Data temp3(6,6);
    found = q1.search(temp3);
    assert(found == false);
    //test for this assignment
    testInsertEmptyQueue();
    testInsertOneElementQueue();
    testInsertThreeElementQueue();
}
