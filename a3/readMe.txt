In this assignment:
       a overload function is added into the class of Queue in the queue.cpp file.
	void insert (Data d,unsigned position);
for the test:
	there are three test functions are added in the testq.cpp file.
	void testInsertEmptyQueue(void)
        void testInsertOneElementQueue(void)
	void testInsertThreeElementQueue(void)
The function of void testInsertEmptyQueue(void) will test the insertion to an empty queue and insertion 
if position is not in the right range.
The function of void testInsertOneElementQueue(void) will test insertion to the begin and end of a queue
that has one element inside.
The function of void testInsertThreeElementQueue(void) will test insertion to the middle of a queue. 
