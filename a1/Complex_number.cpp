/******************************************************************************************************
File : Complex_number.h
Description : This .h file declare a struct called Complex_number, and function prototypes
*******************************************************************************************************/
#include <iostream>
#include <fstream>
#include <iomanip>
#include "Complex_number.h"
#include <stdlib.h>
#include <math.h>
using namespace std;
/**
Purpose: calculate the sum of two complex numbers
@param: two Complex_number and a point to Complex_number
*/
void complex_number_add(Complex_number a, Complex_number b, Complex_number* result)
{
    
    result -> real = a.real + b.real;
    result -> image = a.image + b.image;
}
/**
Purpose: calculate the substractiob of two complex numbers
@param: two Complex_number and a point to Complex_number
*/
void complex_number_sub(Complex_number a, Complex_number b, Complex_number* result)
{
    
    result -> real = a.real - b.real;
    result -> image = a.image - b.image;
}
/**
Purpose: calculate the multiplication of two complex numbers
@param: two Complex_number and a point to Complex_number
*/
void complex_number_mul(Complex_number a, Complex_number b, Complex_number* result)
{
    
    result -> real = (a.real * b.real) - (a.image * b.image);
    result -> image = (a.real * b.image) + (a.image * b.real);
}
/**
Purpose: calculate the division of two complex numbers
@param: two Complex_number and a point to Complex_number, a boolean
@post: can not divided by zero 
*/
void complex_number_div(Complex_number a, Complex_number b, Complex_number* result, bool &check)
{
    float t;
    t = b.real * b.real + b.image*b.image;
    if (fabs(t - 0) < 0.00000000001) check = false;
    result -> real = ((a.real * b.real) - (a.image * (0 - b.image)))/t;
    result -> image = ((a.real * (0 - b.image)) + (a.image * b.real))/t;
}
/**
Purpose: select algorith of two complex number
@param: a char, two Complex_number and a point to Complex_number, a boolean 
*/
void complex_number_cal(char check, Complex_number a, Complex_number b, Complex_number* result, bool &check_div)
{
    switch (check) {
        case 'a':
        case 'A':
            complex_number_add(a, b, result);
            break;
        case 's':
        case 'S':
            complex_number_sub(a, b, result);
            break;
        case 'm':
        case 'M':
            complex_number_mul(a, b, result);
            break;
        case 'd':
        case 'D':
            complex_number_div(a, b, result,check_div);
            break;
            
        default:
            break;
    }
}
/**
Purpose: output the Complex_number
@param:  a point to Complex_number
*/
void complex_number_print(Complex_number *a)
{
	if (a->image < 0)
	{
		cout << a -> real << " - j" << fabs (a -> image) << endl;
	}
	else
	{
		cout << a -> real << " + j" << fabs (a -> image) << endl;
	}
	
}
/**
Purpose: output the Complex_number to a file
@param:  a point to Complex_number and ofstream file
*/
void complex_number_fprint(Complex_number *a,std::ofstream& outdata)
{
	if (a->image < 0)
	{
		outdata << a -> real << " - j" << fabs (a -> image) << endl;
	}
	else if (isnan(a -> real))
	{
		outdata << fabs(a -> real) << " + j" << fabs (a -> image) << endl;
	}
	else
	{
		outdata << a -> real << " + j" << fabs (a -> image) << endl;
	}
}
