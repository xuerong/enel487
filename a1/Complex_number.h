/******************************************************************************************************
File : Complex_number.h
Description : This .h file declare a struct called Complex_number, and function prototypes
*******************************************************************************************************/
#ifndef Complex_number_h
#define Complex_number_h
#include <fstream>

// There are two float valuables in the struct of Complex_number
struct Complex_number
{
	float real;
	float image;
};

void complex_number_add(Complex_number a, Complex_number b, Complex_number* result);
void complex_number_sub(Complex_number a, Complex_number b, Complex_number* result);
void complex_number_mul(Complex_number a, Complex_number b, Complex_number* result);
void complex_number_div(Complex_number a, Complex_number b, Complex_number* result, bool &check);
void complex_number_cal(char check, Complex_number a, Complex_number b, Complex_number* result, bool &check_div);
void complex_number_print(Complex_number *);
void complex_number_fprint(Complex_number *,std::ofstream& outdata);
#endif
