/******************************************************************************************************
File : a1.cpp
Author : Xuerong Wu
Description : This program is used to calculate the addition, substraction, multiplication and division 
              of two complex numbers.
Compiler : gcc version 4.9.2
*******************************************************************************************************/
#include <iostream>
#include <fstream>
#include "Complex_number.h"
using namespace std;
void Cprint(void);
int main (int argc,  char ** argv)
{
	    
	    char check;      //use to input math operation
	    bool isdiv = true;        //use to check if the operarion of division is good
	    Complex_number a;
	    Complex_number b;
	    Complex_number result;
	    Complex_number *presult;
	    presult = &result;        // the point point to the variable of result that will hold algorithm result

	    if (argc == 1) // for interactive mode
	    { 
		        Cprint();
			do 
			{
                            cout << "Enter exp: ";
			    cin >> check ;
			    if (check == 'q' || check == 'Q') break;
			    cin >> a.real >> a.image >>b.real >>b.image;
			    if (check == 'a' || check == 'A' || check == 's' || check == 'S' || check == 'M' || check == 'm' || check == 'd' || check == 'D')
			    {
				complex_number_cal(check, a, b, presult, isdiv);
				if(isdiv == true) complex_number_print(presult); // handle divided by zero
				else cout << "The complex numbers entered can not do division"<<endl;
			    }
			    else cout << "illegal math operation involved" <<endl;
			    isdiv = true;	    
		       }while(1);
		    
	    }
	    if (argc == 3) // for batch mode
	    {
		ifstream indata;
		ofstream outdata;
		indata.open(argv[1]);
		outdata.open(argv[2]);
		do
		{
			indata >> check ;
			if (check == 'q' || check == 'Q') break;
			indata >> a.real >> a.image >>b.real >>b.image;
			if (check == 'a' || check == 'A' || check == 's' || check == 'S' || check == 'M' || check == 'm' || check == 'd' || check == 'D')
			{
				complex_number_cal(check, a, b, presult, isdiv);
				if(isdiv == true) complex_number_fprint(presult,outdata); // handle divided by zero
				else outdata << "The complex numbers entered can not do division"<<endl;
			}
			else outdata << "illegal math operation involved" <<endl;
			isdiv = true;   
		}while(1);
		indata.close();
		outdata.close();
	    }
	return 0;
}
void Cprint(void)
{
	cout <<"COMPLEX CALCULATOR"<<endl<<endl;
	cout <<" Type a letter to specify the arithmetic operator (A, S, M, D)" <<endl;
        cout <<" followed by two complex numbers expressed as pairs of doubles." <<endl;
        cout <<" Type Q to quit."<<endl;
	cout <<endl;
	cout <<endl;
}

